import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  console.log(fighter);
  const fighterImage = document.createElement('img');
  const fighterDescription = document.createElement('div');
  if(fighter) {
    fighterImage.src = fighter.source;
    fighterImage.style.width = '80%';
    fighterElement.style.color = 'white';
    fighterElement.style.fontSize = '32px';
    fighterElement.style.display = 'flex';
    fighterElement.style.flexDirection = 'row';
    fighterDescription.innerText = `${fighter.name}
     attack: ${fighter.attack}
     health: ${fighter.health}
     defense: ${fighter.defense}`;
  }
  fighterElement.append(fighterDescription);
  fighterElement.append(fighterImage);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
