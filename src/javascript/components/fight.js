import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    document.addEventListener('keydown', (e) => {
      if (e.code === controls.PlayerOneAttack && e.code !== controls.PlayerOneBlock && e.code !== controls.PlayerTwoBlock) {
        secondFighter.health -= getDamage(firstFighter, secondFighter);
      }
      if (e.code === controls.PlayerTwoAttack && e.code !== controls.PlayerTwoBlock && e.code !== controls.PlayerOneBlock) {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
      }
      if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }
      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      }
      // resolve the promise with the winner when fight is over
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() * (2 - 1) + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() * (2 - 1) + 1;
  return fighter.defense * dodgeChance;
}
